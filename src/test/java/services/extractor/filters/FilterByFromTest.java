package services.extractor.filters;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilterByFromTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		messageAddress = new InternetAddress("=?UTF-8?B?0KLQtdGB0YI=?= <a@a.ru>");

		message = mock(Message.class);
		when(message.getFrom()).thenReturn(new Address[]{messageAddress});
	}

	@Test(expected = NullPointerException.class)
	public void test_ConstructorShouldGenerateExceptionWhenArgumentIsNull()
	{
		createMailFilter(null);
	}

	@Test(expected = NullPointerException.class)
	public void test_ArgumentOfAcceptMethodCanNotBeNull()
	{
		GenericMailFilter filter = createMailFilter("a@a.ru");

		filter.accept(null);
	}

	@Test
	public void test_AcceptMethodShouldReturnFalseIfFromIsNotEqualsMessageFromFiled()
	{
		GenericMailFilter filter = createMailFilter("b@b.ru");

		boolean result = filter.accept(message);

		assertFalse(result);
	}

	@Test
	public void test_AcceptMethodShouldReturnTrueIfFieldOfFilterEqualsSameFieldOfMessage()
	{
		GenericMailFilter filter = createMailFilter("a@a.ru");

		boolean result = filter.accept(message);

		assertTrue(result);
	}

	private GenericMailFilter createMailFilter(String fieldValue)
	{
		return new FilterByFrom(fieldValue);
	}

	private static Address messageAddress;
	private static Message message;
}
