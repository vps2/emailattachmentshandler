package services.extractor.filters;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.Message;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilterByContentTypeHeadersTest
{
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        message = mock(Message.class);
        when(message.getContentType()).thenReturn("multipart/mixed");
        when(message.isMimeType("multipart/mixed")).thenReturn(true);
    }

    @Test(expected = NullPointerException.class)
    public void test_ConstructorShouldGenerateExceptionWhenArgumentIsNull()
    {
        createMailFilter(null);
    }

    @Test(expected = NullPointerException.class)
    public void test_ArgumentOfAcceptMethodCanNotBeNull()
    {
        GenericMailFilter filter = createMailFilter("multipart/mixed");

        filter.accept(null);
    }

    @Test
    public void test_AcceptMethodShouldReturnFalseIfMimeTypeIsNotEqualsMessageMimeTypeFiled()
    {
        GenericMailFilter filter = createMailFilter("multipart/related");

        boolean result = filter.accept(message);

        assertFalse(result);
    }

    @Test
    public void test_AcceptMethodShouldReturnTrueIfFieldOfFilterEqualsSameFieldOfMessage()
    {
        GenericMailFilter filter = createMailFilter("multipart/mixed");

        boolean result = filter.accept(message);

        assertTrue(result);
    }

    private GenericMailFilter createMailFilter(String fieldValue)
    {
        return new FilterByContentTypeHeaders(fieldValue);
    }

    private static Message message;
}
