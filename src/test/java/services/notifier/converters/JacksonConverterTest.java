package services.notifier.converters;

import org.junit.Test;
import services.notifier.Message;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class JacksonConverterTest
{
	@Test
	public void test_fromMessageMethodShouldConvertMessageIntoJsonString() throws Exception
	{
		String jsonString = converter.fromMessage(message);

		assertThat(jsonString, equalTo(JSON_REPRESENTATION_OF_MESSAGE));
}

	@Test
	public void test_toMessageMethodShouldReturnMessageObjectFromJsonString() throws Exception
	{
		Message received = converter.toMessage(JSON_REPRESENTATION_OF_MESSAGE);

		assertReflectionEquals(message, received);
	}

	private static final String JSON_REPRESENTATION_OF_MESSAGE = "{\"data\":\"test data\"}";
	//
	private final Message message = new Message("test data");
	private final Converter converter = new JacksonConverter();
}