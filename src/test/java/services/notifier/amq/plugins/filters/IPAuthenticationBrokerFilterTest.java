package services.notifier.amq.plugins.filters;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.Connection;
import org.apache.activemq.broker.ConnectionContext;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IPAuthenticationBrokerFilterTest
{
    @BeforeClass
    public static void before()
    {
        broker = mock(Broker.class);
        connection = mock(Connection.class);
        context = mock(ConnectionContext.class);

        when(broker.getBrokerName()).thenReturn("testBroker");
        when(context.getConnection()).thenReturn(connection);
    }

    @Test(expected = SecurityException.class)
    public void test_whenRemoteTcpClientIsNotInListOfAllowedIPsThenThrowsException() throws Exception
    {
        when(connection.getRemoteAddress()).thenReturn("tcp://127.0.0.1:12345");

        List<String> allowedRemoteIPs = Arrays.asList("127.0.0.3");
        IPAuthenticationBrokerFilter filter = new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs);

        filter.addConnection(context, null);
    }

    @Test
    public void test_whenRemoteTcpClientInListOfAllowedIPsThenMethodShouldExecuteWithoutErrors() throws Exception
    {
        when(connection.getRemoteAddress()).thenReturn("tcp://127.0.0.1:12345");

        List<String> allowedRemoteIPs = Arrays.asList("127.0.0.1");
        IPAuthenticationBrokerFilter filter = new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs);

        filter.addConnection(context, null);
    }

    @Test(expected = SecurityException.class)
    public void test_whenVmConnectionIsNotEqualsBrokerNameThenThrowsException() throws Exception
    {
        when(connection.getRemoteAddress()).thenReturn("vm://customBroker#0");

        List<String> allowedRemoteIPs = Collections.EMPTY_LIST;
        IPAuthenticationBrokerFilter filter = new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs);

        filter.addConnection(context, null);
    }

    @Test
    public void test_whenVmConnectionIsEqualsBrokerNameMethodShouldExecuteWithoutErrors() throws Exception
    {
        when(connection.getRemoteAddress()).thenReturn("vm://testBroker#0");

        List<String> allowedRemoteIPs = Collections.EMPTY_LIST;
        IPAuthenticationBrokerFilter filter = new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs);

        filter.addConnection(context, null);
    }

    @Test(expected = SecurityException.class)
    public void test_incomingConnectionFromUnresolvedAddressShouldGenerateException() throws Exception
    {
        when(connection.getRemoteAddress()).thenReturn("tcp://unresolved:12345");

        List<String> allowedRemoteIPs = Arrays.asList("127.0.0.1");
        IPAuthenticationBrokerFilter filter = new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs);

        filter.addConnection(context, null);
    }

    private static ConnectionContext context;
    private static Connection connection;
    private static Broker broker;
}