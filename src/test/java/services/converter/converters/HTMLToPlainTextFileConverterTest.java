package services.converter.converters;

import org.junit.BeforeClass;
import org.junit.Test;
import services.converter.exceptions.InvalidFileFormatException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class HTMLToPlainTextFileConverterTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, URISyntaxException
	{
		URL targetFileURL = HTMLToPlainTextFileConverterTest.class.getClassLoader().getResource(
				  NAME_OF_CORRECT_TARGET_FILE);
		contentOfCorrectTargetFile = new String(Files.readAllBytes(Paths.get(targetFileURL.toURI())));
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_ConstructorShouldGenerateExceptionWhenDestinationPathIsWrong()
	{
		new HTMLToPlainTextFileConverter("!!!");
	}

	@Test(expected = NullPointerException.class)
	public void test_ConstructorShouldGenerateExceptionWhenDestinationPathIsNull()
	{
		new HTMLToPlainTextFileConverter(null);
	}

	@Test(expected = NullPointerException.class)
	public void test_ConstructorShouldGenerateExceptionWhenSourceEncodingIsNull()
	{
		new HTMLToPlainTextFileConverter("", null);
	}

	@Test(expected = InvalidFileFormatException.class)
	public void test_ConvertMethodShouldGenerateExceptionWhenSourceFileHasWrongFormat() throws URISyntaxException,
																															 InvalidFileFormatException
	{
		File sourceFile = getFileObjectFromResource(NAME_OF_WRONG_SOURCE_FILE);
		new HTMLToPlainTextFileConverter(OS_TEMP_DIRECTORY).convert(sourceFile);
	}

	@Test
	public void test_ConvertMethodShouldReturnNewFileInDestinationFolderIfSourceFileCorrect() throws URISyntaxException,
																																	 InvalidFileFormatException,
																																	 IOException
	{
		File sourceFile = getFileObjectFromResource(NAME_OF_CORRECT_SOURCE_FILE);
		FileConverter converter = new HTMLToPlainTextFileConverter(OS_TEMP_DIRECTORY);

		File targetFile = null;
		String contentOfCurrentTargetFile = null;
		try
		{
			targetFile = converter.convert(sourceFile);
			contentOfCurrentTargetFile = new String(Files.readAllBytes(targetFile.toPath()));
		}
		finally
		{
			if(targetFile != null)
			{
				Files.delete(targetFile.toPath());
			}
		}

		assertEquals(contentOfCurrentTargetFile, contentOfCorrectTargetFile);
	}

	private File getFileObjectFromResource(String name) throws URISyntaxException
	{
		URL sourceFileURL = HTMLToPlainTextFileConverterTest.class.getClassLoader().getResource(name);
		return new File(sourceFileURL.toURI());
	}

	private static final String OS_TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");
	//
	private static final String NAME_OF_CORRECT_SOURCE_FILE = "correct.html";
	private static final String NAME_OF_WRONG_SOURCE_FILE = "incorrect.txt";
	private static final String NAME_OF_CORRECT_TARGET_FILE = "20100101.txt";
	//
	private static String contentOfCorrectTargetFile;
}