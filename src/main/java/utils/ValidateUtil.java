package utils;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class ValidateUtil
{
	/**
	 * Осуществляет проверку на существование заданного пути в файловой системе. Если такого пути нет, то возбуждается
	 * исключение.
	 * @param path абсолютный путь к файлу или папке
	 * @return аргумент, переданный для проверки
	 * @throws NullPointerException если переданный аргумент null
	 * @throws IllegalArgumentException если передан не существующий путь
	 * @throws InvalidPathException если передан недопустимый путь
	 */
	public static String pathExists(String path)
	{
		Path p = Paths.get(path);

		return pathExists(p).toString();
	}

	/**
	 * Осуществляет проверку на существование заданного пути в файловой системе. Если такого пути нет, то возбуждается
	 * исключение.
	 * @param path путь к файлу или папке
	 * @return аргумент, переданный для проверки
	 * @throws NullPointerException если переданный аргумент null
	 * @throws IllegalArgumentException если передан не существующий путь
	 * @throws InvalidPathException если передан недопустимый путь
	 */
	public static Path pathExists(Path path)
	{
		if(!Files.exists(path))
		{
			throw new IllegalArgumentException("Path '" + path.toAbsolutePath() + "' is not exists.");
		}

		return path;
	}
}
