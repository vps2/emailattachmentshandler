import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Locale;
import java.util.Scanner;

public class Solution
{
	public static void main(String[] args)
	{
		Locale.setDefault(new Locale("ru_RU"));

		final ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:spring-settings.xml");

		Scanner console = new Scanner(System.in);
		String result;
		while(true)
		{
			result = console.nextLine();
			if("exit".equalsIgnoreCase(result))
			{
				System.exit(0);
			}
		}
	}
}
