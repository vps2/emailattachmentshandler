package services.notifier.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.notifier.Message;
import services.notifier.converters.exceptions.ConverterException;

import java.io.IOException;

import static utils.ClassNameUtil.getCurrentClassName;

public class JacksonConverter implements Converter
{
    public JacksonConverter()
    {
        logger.entry();

        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        mapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false);

        logger.exit();
    }

    @Override
    public String fromMessage(Message message)
    {
        logger.entry(message);

        try
        {
            return logger.exit(mapper.writeValueAsString(message));
        }
        catch(JsonProcessingException ex)
        {
            ConverterException rtException = new ConverterException(ex.getMessage());
            rtException.setLinkedException(ex);
            throw logger.throwing(rtException);
        }
    }

    @Override
    public Message toMessage(String jsonString)
    {
        logger.entry(jsonString);

        try
        {
            return logger.exit(mapper.readValue(jsonString, Message.class));
        }
        catch(IOException ex)
        {
            ConverterException rtException = new ConverterException(ex.getMessage());
            rtException.setLinkedException(ex);
            throw logger.throwing(rtException);
        }
    }

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    //
    private final ObjectMapper mapper = new ObjectMapper();

}
