package services.notifier.converters.exceptions;

public class ConverterException extends RuntimeException
{
    public ConverterException(String reason)
    {
        this(reason, null);
    }

    public ConverterException(String reason, String errorCode)
    {
        super(reason);

        this.errorCode = errorCode;
        this.linkedException = null;
    }

    public Exception getLinkedException()
    {
        return linkedException;
    }

    public void setLinkedException(Exception linkedException)
    {
        this.linkedException = linkedException;
    }


    public String getErrorCode()
    {
        return errorCode;
    }

    private final String errorCode;
    private Exception linkedException;
}
