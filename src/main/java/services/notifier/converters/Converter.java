package services.notifier.converters;

import services.notifier.Message;

public interface Converter
{
    String fromMessage(Message message);
    Message toMessage(String jsonString);
}
