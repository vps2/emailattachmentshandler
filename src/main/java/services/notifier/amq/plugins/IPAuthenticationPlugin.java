package services.notifier.amq.plugins;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.notifier.amq.plugins.filters.IPAuthenticationBrokerFilter;

import java.util.List;

import static utils.ClassNameUtil.getCurrentClassName;

public class IPAuthenticationPlugin implements BrokerPlugin
{
    public IPAuthenticationPlugin(List<String> allowedRemoteIPs)
    {
        logger.entry(allowedRemoteIPs);

        this.allowedRemoteIPs = allowedRemoteIPs;

        logger.exit();
    }

    @Override
    public Broker installPlugin(Broker broker) throws Exception
    {
        logger.entry(broker);

        return logger.exit(new IPAuthenticationBrokerFilter(broker, allowedRemoteIPs));
    }

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    //
    private List<String> allowedRemoteIPs;
}
