package services.notifier.amq.plugins.filters;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.UTF8Control;

import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static utils.ClassNameUtil.getCurrentClassName;

public class IPAuthenticationBrokerFilter extends BrokerFilter
{
    public IPAuthenticationBrokerFilter(Broker next, List<String> allowedRemoteIPs)
    {
        super(next);

        logger.entry(next, allowedRemoteIPs);

        this.allowedRemoteIPs = allowedRemoteIPs;

        logger.exit();
    }

    @Override
    public void addConnection(ConnectionContext context, ConnectionInfo info) throws Exception
    {
        logger.entry(context, info);

        String remoteAddressWithProtocol = context.getConnection().getRemoteAddress();

        Matcher ipMatcher = ipAddressPattern.matcher(remoteAddressWithProtocol);
        Matcher vmMatcher = allowedVmConnectionPattern.matcher(remoteAddressWithProtocol);
        if(ipMatcher.matches())
        {
            String remoteAddress = getRemoteAddress(ipMatcher);
            if(!allowedRemoteIPs.contains(remoteAddress))
            {
                String exceptionMessage =
                        MessageFormat.format(
                                translations.getString("notifier.service.broker-filter.connection_from_remote_ip_refused")
                                , remoteAddress);

                throw logger.throwing(new SecurityException(exceptionMessage));
            }
        }
        else if(vmMatcher.matches())
        {
            String remoteAddress = getRemoteAddress(vmMatcher);
            if(!getBrokerName().equals(remoteAddress))
            {
                String exceptionMessage =
                        MessageFormat.format(
                                translations.getString("notifier.service.broker-filter.connection_from_vm_refused")
                                , remoteAddress);

                throw logger.throwing(new SecurityException(exceptionMessage));
            }
        }
        else
        {
            String exceptionMessage =
                    String.format(translations.getString("notifier.service.broker-filter.invalid_remote_address"));
            throw logger.throwing(new SecurityException(exceptionMessage));
        }

        super.addConnection(context, info);

        logger.exit();
    }

    private String getRemoteAddress(Matcher matcher)
    {
        logger.entry(matcher);

        return logger.exit(matcher.group(1));
    }

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private static final ResourceBundle translations = ResourceBundle.getBundle("translations/messages", new UTF8Control());
    private static final Pattern ipAddressPattern = Pattern.compile("^\\w+://([0-9\\.]*):.*");
    private static final Pattern allowedVmConnectionPattern = Pattern.compile("^vm://(.*)#.*");
    //
    private final List<String> allowedRemoteIPs;
}
