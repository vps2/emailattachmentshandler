package services.notifier;

public interface NotificationService<E>
{
    void notifyClients(E event);
}
