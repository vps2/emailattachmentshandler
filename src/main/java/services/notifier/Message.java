package services.notifier;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Message implements Serializable
{
    public Message()
    {
        this(null);
    }

    public Message(String data)
    {
        this.data = data;
    }

    public String getData()
    {
        return data;
    }

    @Override
    public String toString()
    {
        return "Message{" +
               "data='" + data + '\'' +
               '}';
    }

    private final static long serialVersionUID = 1L;
    @JsonProperty
    private final String data;
}
