package services.notifier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import services.notifier.converters.Converter;
import utils.UTF8Control;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Session;
import java.util.ResourceBundle;

import static utils.ClassNameUtil.getCurrentClassName;

public class NotificationServiceImpl implements NotificationService<Message>
{
    public NotificationServiceImpl(Converter converter, JmsTemplate jmsTemplate)
    {
        logger.entry(converter, jmsTemplate);

        this.converter = converter;
        this.jmsTemplate = jmsTemplate;

        logger.exit();
    }

    @Override
    public void notifyClients(Message event)
    {
        logger.entry(event);

        jmsTemplate.send(new MessageCreator()
        {
            @Override
            public javax.jms.Message createMessage(Session session) throws JMSException
            {
                String json = converter.fromMessage(event);

                BytesMessage message = session.createBytesMessage();
                message.writeBytes(json.getBytes());

                return logger.exit(message);
            }
        });

        logger.info(translations.getString("notifier.service.new_message_was_sent"), event);

        logger.exit();
    }

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private static final ResourceBundle translations = ResourceBundle.getBundle("translations/messages", new UTF8Control());
    //
    private final Converter converter;
    private final JmsTemplate jmsTemplate;
}
