package services.converter.exceptions;

public class InvalidFileFormatException extends Exception
{
	public InvalidFileFormatException()
	{
		super();
	}

	public InvalidFileFormatException(String message)
	{
		super(message);
	}

	public InvalidFileFormatException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public InvalidFileFormatException(Throwable cause)
	{
		super(cause);
	}

	public InvalidFileFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	private static final long serialVersionUID = 10L;
}
