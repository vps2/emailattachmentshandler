package services.converter;

import java.util.List;

public interface FileConversionService
{
	void convert(List<String> files);
}
