package services.converter.converters;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import services.converter.exceptions.InvalidFileFormatException;
import utils.ValidateUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import static utils.ClassNameUtil.getCurrentClassName;

public class HTMLToPlainTextFileConverter implements FileConverter
{
	public HTMLToPlainTextFileConverter(String destinationPath)
	{
		this(destinationPath, DEFAULT_FILE_ENCODING);
	}

	public HTMLToPlainTextFileConverter(String destinationPath, String sourceEncoding)
	{
		logger.entry(destinationPath, sourceEncoding);

		this.destinationPath = checkPath(destinationPath);
		this.sourceEncoding = checkSourceEncoding(sourceEncoding);

		logger.exit();
	}

	public String getSourceEncoding()
	{
		logger.entry();

		return logger.exit(sourceEncoding);
	}

	public void setSourceEncoding(String sourceEncoding)
	{
		logger.entry(sourceEncoding);

		this.sourceEncoding = checkSourceEncoding(sourceEncoding);

		logger.exit();
	}

	public String getDestinationPath()
	{
		logger.entry();
		return logger.exit(destinationPath);
	}

	public void setDestinationPath(String destinationPath)
	{
		logger.entry(destinationPath);

		this.destinationPath = checkPath(destinationPath);

		logger.exit();
	}

	@Override
	public File convert(File source) throws InvalidFileFormatException
	{
		logger.entry(source);

		try
		{
			Document document = Jsoup.parse(source, sourceEncoding);
			Element dateElement = document.select(DATE_ELEMENT_REGEX).first();
			Elements dataElements = document.select(DATA_ELEMENT_REGEX);
			if(dateElement == null || dataElements.isEmpty())
			{
				throw logger.throwing(new InvalidFileFormatException(source.toString()));
			}

			String destinationFileName = getFileName(dateElement.text());
			File destinationFile = new File(String.format(TARGET_FILE_NAME_PATTERN, destinationPath, destinationFileName));
			writeToFile(destinationFile, dataElements);

			return logger.exit(destinationFile);
		}
		catch(IOException ex)
		{
			logger.catching(ex);
		}

		return logger.exit(null);
	}

	private void deleteFile(File file) throws IOException
	{
		logger.entry(file);

		Files.delete(file.toPath());

		logger.exit();
	}

	private void writeToFile(File destinationFile, Elements elements) throws IOException
	{
		logger.entry();

		try(BufferedWriter fileWriter = new BufferedWriter(new FileWriter(destinationFile)))
		{
			for(Element element : elements)
			{
				fileWriter.write(String.format("%s%s", element.text(), System.lineSeparator()));
			}
		}
		catch(IOException ex)
		{
			deleteFile(destinationFile);

			throw logger.throwing(ex);
		}

		logger.exit();
	}

	/**
	 * Вспомогательный метод генерации имени файла.
	 *
	 * @param dateElement строка даты в формате: dd.mm.yyyy
	 * @return строка даты в формате: yyyymmdd
	 */
	private String getFileName(String dateElement)
	{
		logger.entry(dateElement);

		String[] dateElements = dateElement.split("\\.");
		StringBuilder fileName = new StringBuilder();

		for(int elementIndex = dateElements.length - 1; elementIndex >= 0; --elementIndex)
		{
			fileName.append(dateElements[elementIndex]);
		}

		return logger.exit(fileName.toString());
	}

	private String checkSourceEncoding(String sourceEncoding)
	{
		return Validate.notNull(sourceEncoding);
	}

	private String checkPath(String destinationPath)
	{
		return ValidateUtil.pathExists(Validate.notNull(destinationPath));
	}

	private static final String DEFAULT_FILE_ENCODING = "utf-8";
	private static final String DATE_ELEMENT_REGEX = "nobr:matches((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d)";
	private static final String DATA_ELEMENT_REGEX = "nobr:matches(^\\d{10}$)";
	private static final String TARGET_FILE_NAME_PATTERN = "%s/%s.txt";
	private static final Logger logger = LogManager.getLogger(getCurrentClassName());
	//
	private String destinationPath;
	private String sourceEncoding;
}
