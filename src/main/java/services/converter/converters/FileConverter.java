package services.converter.converters;

import services.converter.exceptions.InvalidFileFormatException;

import java.io.File;

public interface FileConverter
{
	/**
	 * Преобразует исходный файл и возвращает ссылку на объект нового.
	 * @param source исходный файл
	 * @return ссылка на объект нового файла.
	 * @throws InvalidFileFormatException
	 */
	default File convert(File source) throws InvalidFileFormatException
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Преобразует исходный файл и сохраняет результат работы в файл - назначение.
	 * @param source исходный файл
	 * @param destination файл для сохранения результатов работы
	 * @throws InvalidFileFormatException
	 */
	default void convert(File source, File destination) throws InvalidFileFormatException
	{
		throw new UnsupportedOperationException();
	}
}
