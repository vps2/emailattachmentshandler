package services.converter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.converter.converters.FileConverter;
import services.converter.exceptions.InvalidFileFormatException;
import services.notifier.Message;
import services.notifier.NotificationService;
import utils.UTF8Control;

import java.io.File;
import java.util.List;
import java.util.ResourceBundle;

import static utils.ClassNameUtil.getCurrentClassName;

public class FileConversionServiceImpl implements FileConversionService
{
    public FileConversionServiceImpl(FileConverter fileConverter)
    {
        this(fileConverter, null);
    }

    public FileConversionServiceImpl(FileConverter fileConverter, NotificationService<Message> notifier)
    {
        logger.entry(fileConverter, notifier);

        this.fileConverter = fileConverter;
        this.notifier = notifier;

        logger.exit();
    }

    @Override
    public void convert(List<String> files)
    {
        logger.entry(files);

        for(String fileName : files)
        {
            File sourceFile = new File(fileName);

            try
            {
                File newFile = fileConverter.convert(sourceFile);
                if(newFile != null && newFile.exists())
                {
                    logger.info(translations.getString("converter.service.create_new_file"), newFile, sourceFile);
                    if(notifier != null)
                    {
                        notifier.notifyClients(new Message(newFile.getName()));
                    }
                }
            }
            catch(InvalidFileFormatException ex)
            {
                logger.catching(ex);
            }
        }

        logger.exit();
    }

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private static final ResourceBundle translations = ResourceBundle.getBundle("translations/messages", new UTF8Control());
    //
    private FileConverter fileConverter;
    private NotificationService<Message> notifier;
}
