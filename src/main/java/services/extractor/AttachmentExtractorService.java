package services.extractor;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface AttachmentExtractorService
{
	/**
	 * Метод извлечения вложений из почтового сообщения.
	 * @param message - почтовое сообщение с вложениями
	 * @return список абсолютных путей к извлечённым из почтового сообщения файлам.
	 * @throws MessagingException
	 * @throws IOException
	 */
	List<String> extract(Message message) throws MessagingException, IOException;
}
