package services.extractor.filters;

import javax.mail.Message;
import javax.mail.MessagingException;

public class FilterBySubject extends GenericMailFilter
{
    public FilterBySubject(String expectedFieldValue)
    {
        super(expectedFieldValue);
    }

    @Override
    protected boolean isAccepted(Message email) throws MessagingException
    {
        return getFieldExpectedValue().equalsIgnoreCase(getFieldActualValue(email));
    }

    @Override
    protected String getFieldActualValue(Message email) throws MessagingException
    {
        return email.getSubject();
    }

    @Override
    protected String getFilteredFieldName()
    {
        return FILTERED_FIELD_NAME;
    }

    private static final String FILTERED_FIELD_NAME = "Subject";
}