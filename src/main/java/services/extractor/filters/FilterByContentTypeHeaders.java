package services.extractor.filters;

import javax.mail.Message;
import javax.mail.MessagingException;

public class FilterByContentTypeHeaders extends GenericMailFilter
{
    public FilterByContentTypeHeaders(String expectedFieldValue)
    {
        super(expectedFieldValue);
    }

    @Override
    protected boolean isAccepted(Message email) throws MessagingException
    {
        return email.isMimeType(getFieldExpectedValue());
    }

    @Override
    protected String getFieldActualValue(Message email) throws MessagingException
    {
        return email.getContentType();
    }

    @Override
    protected String getFilteredFieldName()
    {
        return FILTERED_FIELD_NAME;
    }

    private static final String FILTERED_FIELD_NAME = "Content Type";
}
