package services.extractor.filters;

import org.apache.commons.lang3.StringUtils;

import javax.mail.Message;
import javax.mail.MessagingException;

public class FilterByFrom extends GenericMailFilter
{
    public FilterByFrom(String expectedFieldValue)
    {
        super(expectedFieldValue);
    }

    @Override
    protected boolean isAccepted(Message email) throws MessagingException
    {
        return StringUtils.containsIgnoreCase(getFieldActualValue(email), getFieldExpectedValue());
    }

    @Override
    protected String getFieldActualValue(Message email) throws MessagingException
    {
        return email.getFrom()[0].toString();
    }

    @Override
    protected String getFilteredFieldName()
    {
        return FILTERED_FIELD_NAME;
    }

    private static final String FILTERED_FIELD_NAME = "From";
}
