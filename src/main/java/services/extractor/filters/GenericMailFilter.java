package services.extractor.filters;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.UTF8Control;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.ResourceBundle;

import static utils.ClassNameUtil.getCurrentClassName;

public abstract class GenericMailFilter
{
	public GenericMailFilter(String expectedValueOfField)
	{
		logger.entry(expectedValueOfField);

		this.expectedValueOfField = Validate.notNull(expectedValueOfField);

		logger.exit();
	}

	public boolean accept(Message email)
	{
		logger.entry(email);

		Validate.notNull(email);

		try
		{
			if(isAccepted(email))
			{
				return logger.exit(true);
			}
			else
			{
				logger.warn(translations.getString("extractor.service.not_passed_filtering"),
							getFilteredFieldName(),
							getFieldExpectedValue(),
							getFieldActualValue(email));
			}
		}
		catch(MessagingException ex)
		{
			logger.error(translations.getString("extractor.service.email_processing_error"), ex);
		}

		return logger.exit(false);
	}

	protected String getFieldExpectedValue()
	{
		return expectedValueOfField;
	}

	protected abstract boolean isAccepted(Message email) throws MessagingException;
	protected abstract String getFieldActualValue(Message email) throws MessagingException;
	protected abstract String getFilteredFieldName();

	private static final Logger logger = LogManager.getLogger(getCurrentClassName());
	private static final ResourceBundle translations = ResourceBundle.getBundle("translations/messages", new UTF8Control());
	//
	private String expectedValueOfField;
}
