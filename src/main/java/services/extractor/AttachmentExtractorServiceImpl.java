package services.extractor;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.UTF8Control;
import utils.ValidateUtil;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static utils.ClassNameUtil.getCurrentClassName;

public class AttachmentExtractorServiceImpl implements AttachmentExtractorService
{
	public AttachmentExtractorServiceImpl(String destination)
	{
		logger.entry(destination);

		this.destination = ValidateUtil.pathExists(Validate.notNull(destination));

		logger.exit();
	}

	@Override
	public List<String> extract(Message message) throws MessagingException, IOException
	{
		logger.entry(message);

		Validate.notNull(message);

		Multipart multiPart = (Multipart) message.getContent();
		int numberOfParts = multiPart.getCount();

		List<String> extractedFiles = new ArrayList<>();

		for(int partNumber = 0; partNumber < numberOfParts; ++partNumber)
		{
			try
			{
				MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(partNumber);
				if(Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()))
				{
					String absoluteFileName = destination + File.separator +  MimeUtility.decodeText(bodyPart.getFileName());
					bodyPart.saveFile(absoluteFileName);

					Path path = Paths.get(absoluteFileName);
					logger.info(translations.getString("extractor.service.save_file"), path.getFileName(), destination);
					extractedFiles.add(absoluteFileName);
				}
			}
			catch(MessagingException | IOException ex)
			{
				logger.error(translations.getString("extractor.service.email_processing_error"), ex);
			}
		}

		return logger.exit(extractedFiles);
	}

	private static final Logger logger = LogManager.getLogger(getCurrentClassName());
	private static final ResourceBundle translations = ResourceBundle.getBundle("translations/messages", new UTF8Control());
	//
	private String destination;
}
